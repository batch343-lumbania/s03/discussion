-- [SECTION] CREATION OF RECORDS / INSERT
    -- INSERT INTO table_name (columns_in table) VALUES (values_per_column);

    INSERT INTO artists (name) VALUES ("Rivermaya");
    INSERT INTO artists (name) VALUES ("NewJeans");

    -- Multiple inserts
    INSERT INTO artists (name) VALUES ("Taylor Swift"), ("Big Sean");

    INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Trip", "1996-1-1", 1);

    INSERT INTO albums (album_title, date_released, artist_id) VALUES ("Get Up", "2023-07-21", 2), ("New Jeans", "2022-08-01", 2), ("Dark Sky Paradise", "2015-02-04", 4);

-- [SECTION] Read / Retrieving records from db
    -- SELECT * FROM table_name;

    SELECT * FROM artists;

    -- Adding record in the songs table

    INSERT INTO songs (song_name, length, album_id, genre) VALUES ("Kundiman", 234, 1, "OPM");

    INSERT INTO songs (song_name, length, album_id, genre) VALUES ("ASAP", 214, 2, "K-POP"), ("ETA", 231, 2, "K-POP"), ("Super Shy", 234, 2, "K-POP"), ("Hype Boy", 259, 3, "K-POP");


    -- Retrieve OPM songs only
    SELECT * FROM songs WHERE genre = "OPM";


    -- We can use AND and OR operator for multiple expressions
    SELECT * FROM songs where genre = "OPM" AND length > 234;

-- [SECTION] Updating record
    -- Syntax :
        -- UPDATE table_name SET column = value_to_be WHERE condition;
    UPDATE songs SET length = 240 WHERE song_name = "Kundiman";

-- [SECTION] Deleting Record
    -- Syntax:
        -- DELETE FROM table_name WHERE condition;
    DELETE FROM songs WHERE length = 240;